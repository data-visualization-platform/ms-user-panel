FROM node:8.13-alpine
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./

RUN npm install --silent

COPY . ./
CMD ["npm", "run", "start"]
